function myFunction() {
    // Declare variables 
    var input, filter, table, tr, td, i, occurrence;

    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
   for (i = 0; i < tr.length; i++) {
       occurrence = false; // Only reset to false once per row.
       td = tr[i].getElementsByTagName("td");
       for(var j=0; j< td.length; j++){                
           currentTd = td[j];
           if (currentTd ) {
               if (currentTd.innerHTML.toUpperCase().indexOf(filter) > -1) {
                   tr[i].style.display = "";
                   occurrence = true;
               } 
           }
       }
       if(!occurrence){
           tr[i].style.display = "none";
       } 
   }
 }