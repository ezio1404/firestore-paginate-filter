    // Your web app's Firebase configuration
    var firebaseConfig = {
        apiKey: "AIzaSyAGw8e-UKQJjIpy_b_fG6W2xnv8e1NrUVc",
        authDomain: "firestore-tut-95e1a.firebaseapp.com",
        databaseURL: "https://firestore-tut-95e1a.firebaseio.com",
        projectId: "firestore-tut-95e1a",
        storageBucket: "firestore-tut-95e1a.appspot.com",
        messagingSenderId: "997911532088",
        appId: "1:997911532088:web:9dfacb7759402d8b"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);

    const db = firebase.firestore();
    //   db.settings({timestampsInSnapshots: true});
    const cafeList = document.querySelector('#cafe-list');

    function renderCafe(doc) {
        //creating html 
        let tr = document.createElement('tr');
        // let id = document.createElement('td');
        let name = document.createElement('td');
        let city = document.createElement('td');
        // let cross = document.createElement('td');
        let actionHolder = document.createElement('td');
        let deleteBtn = document.createElement('button');

        //identifying the tag in the front end
        tr.setAttribute('class', 'items');

        // id.textContent = doc.id;
        name.textContent = doc.data().name;
        city.textContent = doc.data().city;
        deleteBtn.textContent = 'x';
        actionHolder.setAttribute('data-id', doc.id);
        // tr.appendChild(id);
        tr.appendChild(name);
        tr.appendChild(city);
        actionHolder.appendChild(deleteBtn);
        tr.appendChild(actionHolder);

        cafeList.appendChild(tr);

        // dleeting data
        deleteBtn.addEventListener('click', (e) => {
            e.stopPropagation();
            let id = e.target.parentElement.getAttribute('data-id');
            db.collection('cafes').doc(id).delete();
        });


    }

    db.collection('cafes').orderBy('city').onSnapshot(snapshot => {
        let changes = snapshot.docChanges();
        // console.log(changes);
        changes.forEach(change => {
            if (change.type == 'added') {
                renderCafe(change.doc);
            } else if (change.type == 'removed') {
                let li = cafeList.querySelector('[data-id= ' + change.doc.id + ']');
                cafeList.removeChild(li);
            }
        });
    });